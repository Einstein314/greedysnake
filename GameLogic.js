var BLOCK_SIZE = 20 //每一格20px (原座標放大20倍)
var BLOCK_COUNT = 20 //座標0~19

var gameInterval //每隔一段時間重新計算 再畫出畫面

var snake
var apple
var score

var is_click_Q
var original_x
var original_y

window.onload = onPageLoaded //當頁面被載入時 執行onPageLoded
function onPageLoaded() {
    document.addEventListener("keydown", handleKeyDown) //在原始碼新增keydown聆聽事件 並執行handleKeyDown   
}

function handleKeyDown(event) { //控制蛇
    switch (event.keyCode) {
        case 37: //左
            if (snake.direction.y != 0) { //note: ===嚴格等於(型別不同就False) ==一般等於(會先把型別用成一致再比較) 
                snake.direction.x = -1
                snake.direction.y = 0
            }
            break;
        case 38: //上
            if (snake.direction.x != 0) {
                snake.direction.x = 0
                snake.direction.y = -1
            }
            break;
        case 39: //右
            if (snake.direction.y != 0) {
                snake.direction.x = 1
                snake.direction.y = 0
            }
            break;
        case 40: //下
            if (snake.direction.x != 0) {
                snake.direction.x = 0
                snake.direction.y = 1
            }
            break;
        case 81: //Q建 (自己新增暫停功能)  因為空白鍵有內建重新開始
            if (is_click_Q === 0) {
                original_x = snake.direction.x  //必須放在這裡 才不會被更新原方向向量
                original_y = snake.direction.y
                snake.direction.x = 0
                snake.direction.y = 0
                is_click_Q = 1   //但movesnake會新增newBlock在蛇頭 導致死掉
            } else {
                snake.direction.x = original_x
                snake.direction.y = original_y
                is_click_Q = 0
            }

    }

}

function gameStart() {
    clearInterval(gameInterval) //先做清除
    is_click_Q = 0
    snake = {  //初始化蛇
        body: [  //列表
            { x: BLOCK_COUNT / 2, y: BLOCK_COUNT / 2 } //座標中心
        ],
        size: 5,
        direction: { x: 0, y: -1 } //方向向上
    }

    putApple() //初始化蘋果

    updateScore(0)

    gameInterval = setInterval(gameRoutine, 100) //每0.1秒執行一次gameRoutine() //給值後就開始執行    
    //記得clearInterval 否則定時器會越走越快(因為原來的定時器沒有被銷毀 所以就會一起重新執行gameRoutine 0.1s-->0.05s-->0.025s...)
    //每初始化一次定時器 就會增速
}

function gameRoutine() {   //重新計算 畫出畫面
    if (is_click_Q == 0) { //暫停時不執行(才不會被newBlock弄死 或 方向鍵控制)
        moveSnake() //移動蛇
    }

    if (isSnakeDead()) { //移動完蛇後 要確認蛇是否死掉
        gameOver()
        return //不用再畫畫面 直接回傳
    }

    if (snake.body[0].x === apple.x &&  //蛇移動完且沒有死掉 --> 確認有沒有吃到蘋果
        snake.body[0].y === apple.y) {
        eatApple()
    }

    updateCanvas() //畫畫面
}

function moveSnake() {
    var newBlock = {
        x: snake.body[0].x + snake.direction.x,
        y: snake.body[0].y + snake.direction.y
        // 在蛇頭(snake.body[0])的位置 新增移動方向上的新區塊 (但尾巴的區塊還在 要刪除)
    }

    snake.body.unshift(newBlock) // unshift在列表由前新增  push在列表後由尾新增

    while (snake.body.length > snake.size) { //當長度大於應有的size則由尾巴刪除多於區塊
        snake.body.pop() //pop() 由列表尾端彈出
    }
}

function isSnakeDead() {
    //撞牆
    if (snake.body[0].x < 0) {
        return true
    } else if (snake.body[0].x >= BLOCK_COUNT) { //如果蛇頭超出x座標(座標用BLOCK_COUNT即可，因為畫的時候會放大20倍，所以座標和畫面分開看)
        return true
    } else if (snake.body[0].y < 0) {
        return true
    } else if (snake.body[0].y >= BLOCK_COUNT) { //座標0~19 所以y座標=20時 會畫超出Canvas
        return true
    }

    //撞到自己
    for (var i = 1; i < snake.body.length; i++) { //迴圈從1開始 因為0已經是蛇頭了
        if (snake.body[0].x === snake.body[i].x &&
            snake.body[0].y === snake.body[i].y) {
            return true
        }
    }

    return false
}

function gameOver() {
    clearInterval(gameInterval) //將setInterval內執行的gameInterval清除
}

function putApple() {
    apple = {
        x: Math.floor(Math.random() * BLOCK_COUNT), //0<= Math.random() <1 所以放大20倍就是0~19 //Math.floor()取高斯
        y: Math.floor(Math.random() * BLOCK_COUNT)
    }

    for (var i = 0; i < snake.body.length; i++) { //蘋果放在蛇身上 
        if (apple.x === snake.body[i].x &&
            apple.y === snake.body[i].y) {
            putApple()
            break //找到就break
        }
    }
}

function eatApple() {
    snake.size += 1
    putApple() //吃到之後才重新放
    updateScore(score + 1)
}

function updateScore(newScore) {
    score = newScore
    document.getElementById("score_id").innerHTML = score //插入文字
}

function updateCanvas() { //畫畫面(都是在背景上畫)

    //畫背景
    var canvas = document.getElementById("canvas_id")
    var context = canvas.getContext("2d")

    context.fillStyle = "black"
    context.fillRect(0, 0, canvas.width, canvas.height) //座標會被重新定義(因為有元素擋住) //(左上x座標, 左上y左標, 矩形寬度, 矩形長度)

    //畫蛇 (在原有的背景上覆蓋上去)
    context.fillStyle = "lime"
    for (var i = 0; i < snake.body.length; i++) {
        context.fillRect( //畫畫面時要使用像素的座標 所以要放大20倍
            snake.body[i].x * BLOCK_SIZE + 1, //座標放大20倍 才會在canvas中央
            snake.body[i].y * BLOCK_SIZE + 1,
            BLOCK_SIZE - 1,
            BLOCK_SIZE - 1  //上下左右都往內縮1px (自己畫畫看)
        )
    }

    //畫蘋果
    context.fillStyle = "red"
    context.fillRect(
        apple.x * BLOCK_SIZE + 1,
        apple.y * BLOCK_SIZE + 1,
        BLOCK_SIZE - 1,
        BLOCK_SIZE - 1
    )

}


